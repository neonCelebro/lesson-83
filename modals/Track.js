const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const trackSchema = new Schema({
    name: {type: String, required : true, unique: true},
    album: {type: Schema.Types.ObjectId, ref: 'Album', required: true},
    time: {type: String}
});

const Track = mongoose.model('Track', trackSchema);

module.exports = Track;