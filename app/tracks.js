const express = require('express');
const Track = require('../modals/Track');

const router = express.Router();

const createRouter = () => {

    router.post('/', (req, res) => {
        const track = req.body;
        const tracks = new Track(track);
        tracks.save()
            .then(result => res.send(result))
            .catch((err) => res.send("some not work " + err));
    });

    router.get('/', (req, res) => {
        if (req.query.album) {
            Track.find({author: req.query.album}, (error, result) => {
                if (error) res.status(404).send(error);
                if (result) res.send(result);
            }).populate('album')
        }
        else {
            Track.find().populate('album')
                .then(result => res.send(result))
                .catch((error) => res.send(error))
        }
    });

    return router;
};

module.exports = createRouter;