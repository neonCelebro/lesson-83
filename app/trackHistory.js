const express = require('express');
const TrackHistory = require('../modals/TrackHistory');
const User = require('../modals/User');
const date = new Date();

const createRouter = () => {
    const router = express.Router();

    router.post('/', async (req, res) => {
        const token = req.get('token');
        const isUser = await User.findOne({token});
        if (isUser) {
            const trackHistory = new TrackHistory();
            trackHistory.datetime = date.toISOString();
            trackHistory.user = isUser._id;
            trackHistory.track = req.body.track;
            trackHistory.save()
                .then(result => res.send(result))
                .catch(err => res.send(err));
        }
        else res.sendStatus(401);
    });

    return router;
};

module.exports = createRouter;