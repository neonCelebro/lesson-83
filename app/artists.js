const express = require('express');
const nanoid = require('nanoid');
const path = require('path');
const multer = require('multer');
const Artist = require('../modals/Artist');
const config = require('../config');


const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath + '/artists');
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {

    router.post('/', upload.single('image'), (req, res) => {
        const artist = req.body;

        if (req.file) {
            artist.image = req.file.filename;
        } else {
            artist.image = "artist.img";
        }
        const artists = new Artist(artist);
        artists.save()
            .then(result => res.send(result))
            .catch((err) => res.send(err));
    });

    router.get('/', (req, res) => {
       Artist.find()
           .then (result => res.send(result))
           .catch((error) => res.send(error))
    });

    return router;
};

module.exports = createRouter;